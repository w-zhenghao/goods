package com.goods.business.mapper;

import com.goods.common.vo.business.ProductCategoryVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * projectName: goods
 *
 * @author: 韦政浩
 * time: 2022/12/2 19:48 周五
 * description:
 */

@Mapper
public interface ProductCategoryVOMapper extends tk.mybatis.mapper.common.Mapper<ProductCategoryVO> {
}
