package com.goods.business.mapper;

import com.goods.common.model.business.ProductCategory;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * projectName: goods
 *
 * @author: 韦政浩
 * time: 2022/12/2 20:57 周五
 * description:
 */

@Mapper
public interface ProductCategoryMapper extends tk.mybatis.mapper.common.Mapper<ProductCategory> {
}
