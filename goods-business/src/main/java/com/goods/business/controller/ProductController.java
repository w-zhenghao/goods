package com.goods.business.controller;

import com.goods.business.service.ProductTreeService;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.business.ProductCategoryVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * projectName: goods
 *
 * @author: 韦政浩
 * time: 2022/12/2 13:20 周五
 * description:
 */
@RestController
@RequestMapping("/business/productCategory/")
public class ProductController {

///business/productCategory/categoryTree?pageNum=1&pageSize=5

    @Autowired
    private ProductTreeService productTreeService;
    //物资类别
    @GetMapping("categoryTree")
    public  ResponseBean<PageVO<ProductCategoryTreeNodeVO>>productTree(@RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum,
                                                          @RequestParam(value="pageSize")Integer pageSize,
                                                          ProductCategoryTreeNodeVO productCategoryTreeNodeVO) {
PageVO<ProductCategoryTreeNodeVO>pageVO=productTreeService.productTree(pageNum,pageSize,productCategoryTreeNodeVO);
return ResponseBean.success(pageVO);
    }

    //   post  /business/productCategory/add

//    @PostMapping("add")
//    public ResponseBean add(){
//        productTreeService.add();
//        return ResponseBean.success();
//    }

}
