package com.goods.business.service;

import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.stereotype.Repository;

/**
 * projectName: goods
 *
 * @author: 韦政浩
 * time: 2022/12/2 20:47 周五
 * description:
 */

@Repository
public interface ProductTreeService {
    /**
     * 获取分类数据
     * @param pageNum
     * @param pageSize
     * @param productCategoryTreeNodeVO
     * @return
     */
    PageVO<ProductCategoryTreeNodeVO> productTree(Integer pageNum, Integer pageSize, ProductCategoryTreeNodeVO productCategoryTreeNodeVO);

}
