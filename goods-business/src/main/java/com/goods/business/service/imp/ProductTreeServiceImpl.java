package com.goods.business.service.imp;

import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.ProductCategoryMapper;
import com.goods.business.service.ProductTreeService;
import com.goods.common.model.business.ProductCategory;
import com.goods.common.utils.CategoryTreeBuilder;
import com.goods.common.utils.ListPageUtils;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * projectName: goods
 *
 * @author: 韦政浩
 * time: 2022/12/2 20:47 周五
 * description:
 */
@Service
public class ProductTreeServiceImpl implements ProductTreeService {

    @Autowired
    private ProductCategoryMapper productCategoryMapper;
    /**
     * 获取分类数据
     *
     * @param pageNum
     * @param pageSize
     * @param productCategoryTreeNodeVO
     * @return
     */
    @Override
    public PageVO<ProductCategoryTreeNodeVO> productTree(Integer pageNum, Integer pageSize, ProductCategoryTreeNodeVO productCategoryTreeNodeVO) {
            //tk-mybatis
        Example example = new Example(ProductCategory.class);
        if (productCategoryTreeNodeVO.getName()!=null &&!"".equals(productCategoryTreeNodeVO.getName())){
            example.createCriteria().andLike("name","%"+productCategoryTreeNodeVO.getName()+"%");
        }
        //进行方法查找定义
        List<ProductCategory> productCategories = productCategoryMapper.selectByExample(example);
        //集合
      List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOS = new ArrayList<>();
        //进行判断集合是否为空
        if (!CollectionUtils.isEmpty(productCategories)){
            //进行遍历
            for (ProductCategory productCategory : productCategories) {
                //新创建一个分类
                ProductCategoryTreeNodeVO treeNodeVO1 = new ProductCategoryTreeNodeVO();
                //拷贝
                BeanUtils.copyProperties(productCategory,treeNodeVO1);
                //放入想创建的集合中
                productCategoryTreeNodeVOS.add(treeNodeVO1);
            }
        }
        //分类数据集合
        List<ProductCategoryTreeNodeVO> build = CategoryTreeBuilder.build(productCategoryTreeNodeVOS);
        //当前页  每页记录数 和分类数据放入一个page里面
        List<ProductCategoryTreeNodeVO> page = ListPageUtils.page(build, pageSize, pageNum);
        //对分类数据进行封装
        PageInfo<ProductCategoryTreeNodeVO> info = new PageInfo<>(build);
        //返回数据
        return new PageVO<>(info.getTotal(),page);
    }
}
